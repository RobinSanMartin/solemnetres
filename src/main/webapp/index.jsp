<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1 align="center">Robin San Martin</h1>
        <h1 align="center">Seccion 6</h1>
        <h1 align="center">Api Personas</h1>

        <table align="center" border="1" bordercolor="blue">
            <th colspan="3">Endpoints</th>
            <tr>
                <td>Funcion</td>
                <td>Metodo</td>
                <td>Url</td>
            </tr>
            <tr>
                <td>Listar Personas</td>
                <td>GET</td>
                <td>https://solemnetres.herokuapp.com/api/persona</td>
            </tr>
            <tr>
                <td>Agregar Persona</td>
                <td>POST</td>
                <td>https://solemnetres.herokuapp.com/api/persona</td>
            </tr>
            <tr>
                <td>Actualizar Datos</td>
                <td>PUT</td>
                <td>https://solemnetres.herokuapp.com/api/persona</td>
            </tr>
            <tr>
                <td>Eliminar Persona</td>
                <td>DELETE</td>
                <td>https://solemnetres.herokuapp.com/api/persona/{ideliminar}</td>
            </tr>
            <tr>
                <td>Consultar Persona</td>
                <td>GET</td>
                <td>https://solemnetres.herokuapp.com/api/persona/{idconsultar}</td>
            </tr>

        </table>
    </body>
</html>
