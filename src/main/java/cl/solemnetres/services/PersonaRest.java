package cl.solemnetres.services;

import cl.solemnetres.dao.PersonaJpaController;
import cl.solemnetres.dao.exceptions.NonexistentEntityException;
import cl.solemnetres.entity.Persona;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("persona")
public class PersonaRest {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarPersona(){
        PersonaJpaController dao=new PersonaJpaController();
        List<Persona> persona =dao.findPersonaEntities();
        return Response.ok(200).entity(persona).build();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Persona persona){
        PersonaJpaController dao=new PersonaJpaController();
        try {
            dao.create(persona);
        } catch (Exception ex) {
            Logger.getLogger(PersonaRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(persona).build();
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Persona persona){
        PersonaJpaController dao=new PersonaJpaController();
        try {
            dao.edit(persona);
        } catch (Exception ex) {
            Logger.getLogger(PersonaRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(persona).build();
    }
    
    @DELETE
    @Path("/{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("ideliminar") String ideliminar){
        PersonaJpaController dao=new PersonaJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(PersonaRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Persona Eliminada").build();
        
    }
    
    @GET
    @Path("/{idconsultar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPersona(@PathParam("idconsultar") String idconsultar){
        PersonaJpaController dao=new PersonaJpaController();
        Persona persona = dao.findPersona(idconsultar);
        return Response.ok(200).entity(persona).build();
    }
     
}
